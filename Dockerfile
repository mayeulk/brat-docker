
FROM httpd

RUN \
    apt-get clean && \
    apt-get update && \
    apt-get install -y \
        ca-certificates \
        python \
        wget

RUN \
    cd htdocs && \
    wget http://weaver.nlplab.org/~brat/releases/brat-v1.3_Crunchy_Frog.tar.gz && \
    tar -xvzf *.tar.gz && \
    rm *.tar.gz && \
    mv brat*/ brat/

ADD httpd.conf /usr/local/apache2/conf/
ADD install.sh /usr/local/apache2/htdocs/brat/

RUN /usr/local/apache2/htdocs/brat/install.sh admin password admin@example.com
